import i18next from 'i18next';
import en from './en.json';
import es from './es.json';
import hi from './hi.json';
import fr from './fr.json';
import {initReactI18next} from 'react-i18next';

i18next.use(initReactI18next).init({
  lng: 'en',
  resources: {
    en: en,
    hi: hi,
    fr: fr,
    es: es,
  },
  react: {
    useSuspense: false,
  },
});

export default i18next;
