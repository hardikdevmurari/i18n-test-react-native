import React from 'react';
import {useTranslation} from 'react-i18next';
import {View, Text, Button} from 'react-native';
import SwitchSelector from 'react-native-switch-selector';

const options = [
  {label: 'English', value: 'en'},
  {label: 'Hindi', value: 'hi'},
  {label: 'Spanish', value: 'es'},
];

const App = () => {
  const {t, i18n} = useTranslation();
  return (
    <View>
      <View style={{justifyContent: 'flex-end'}}>
        <SwitchSelector
          options={options}
          onPress={value => {
            i18n.changeLanguage(value);
          }}
        />
      </View>

      <View style={{alignContent: 'center', justifyContent: 'center'}}>
        <Text
          style={{
            fontSize: 20,
            alignItems: 'center',
            textAlign: 'center',
            justifyContent: 'center',
          }}>
          {t('WelcomeText')}
        </Text>
      </View>
    </View>
  );
};

export default App;
